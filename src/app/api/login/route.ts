import { uses } from "@/utils";
import jwt from "jsonwebtoken";
import { NextRequest, NextResponse } from "next/server";

export async function POST(req: NextRequest, res: NextResponse) {
  try {
    const data = await req.json();
    const { email, password } = data;
    const user = uses.find((u) => u.email === email && u.password === password);
    if (user) {
      const token = jwt.sign(
        {
          name: user.name,
          email: user.email,
        },
        "secret",
        { expiresIn: "7d" }
      );
      return NextResponse.json({
        success: true,
        token,
        name: user.name,
      });
    }
    return NextResponse.json({
      success: false,
      message: "Invalid email or password",
    });
  } catch (e) {
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    });
  }
}
