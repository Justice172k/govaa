import jwt from "jsonwebtoken";
import { NextRequest, NextResponse } from "next/server";

export async function POST(req: NextRequest, res: NextResponse) {
  try {
    const data = await req.json();
    const { token, email } = data;

    const payload = jwt.verify(token, "secret");
    return NextResponse.json({
      success: true,
      status: 200,
      payload,
    });
  } catch (e) {
    return NextResponse.json({
      success: false,
      status: 403,
      message: "Unauthorized",
    });
  }
}
