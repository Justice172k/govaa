export const uses = [
  {
    email: "user1@gov.com",
    password: "user1",
    name: "User 1",
  },
  {
    email: "user2@gov.com",
    password: "user2",
    name: "User 2",
  },
  {
    email: "user3@gov.com",
    password: "user3",
    name: "User 3",
  },
  {
    email: "user4@gov.com",
    password: "user4",
    name: "User 4",
  },
  {
    email: "user5@gov.com",
    password: "user5",
    name: "User 5",
  },
  {
    email: "user6@gov.com",
    password: "user6",
    name: "User 6",
  },
];
